from django.contrib.auth import views as auth_views
from django.contrib.auth.models import auth
from django.urls import path
from django.conf import settings
from django.contrib.auth import views as auth_views
from . import views
from .forms import LoginForm

app_name = 'core'

urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact'),
    path('signup/', views.signup, name='signup'),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('contact_submit/', views.contact_submit, name='contact_submit'),
    path('login/', auth_views.LoginView.as_view(template_name='core/login.html',
         authentication_form=LoginForm), name='login'),
]
