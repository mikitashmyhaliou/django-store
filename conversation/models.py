from django.contrib.auth.models import User
from django.db import models

from item.models import Item


class Conversation(models.Model):
    item = models.ForeignKey(
        Item, related_name='conversations', on_delete=models.CASCADE, verbose_name='przedmiot')
    members = models.ManyToManyField(
        User, related_name='conversations', verbose_name='członkowie')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='utworzono')
    modified_at = models.DateTimeField(
        auto_now=True, verbose_name='zmodyfikowano')

    class Meta:
        ordering = ('-modified_at',)
        verbose_name = 'rozmowa'
        verbose_name_plural = 'rozmowy'


class ConversationMessage(models.Model):
    conversation = models.ForeignKey(
        Conversation, related_name='messages', on_delete=models.CASCADE, verbose_name='rozmowa')
    content = models.TextField(verbose_name='treść')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='utworzono')
    created_by = models.ForeignKey(
        User, related_name='created_messages', on_delete=models.CASCADE, verbose_name='utworzone przez')

    class Meta:
        verbose_name = 'wiadomość'
        verbose_name_plural = 'wiadomości'
