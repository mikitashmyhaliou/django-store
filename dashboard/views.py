from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from item.models import Item


@login_required
def index(request):
    items = Item.objects.filter(created_by=request.user)

    page_number = request.GET.get('page')
    paginator = Paginator(items, 10)  # Show 9 items per page

    try:
        items = paginator.page(page_number)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)
    return render(request, 'dashboard/index.html', {
        'items': items,
    })
