from django import forms
from django.utils.translation import gettext_lazy as _  # Import for translation

from .models import Item

INPUT_CLASSES = 'w-full py-4 px-6 rounded-xl border'


class NewItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('category', 'name', 'description', 'price', 'image',)
        widgets = {
            'category': forms.Select(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Wybierz kategorię...')
            }),
            'name': forms.TextInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Nazwa...')
            }),
            'description': forms.Textarea(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Opis...')
            }),
            'price': forms.TextInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Cena...')
            }),
            'image': forms.FileInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Wybierz zdjęcie...')
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].label = _('Kategoria')
        self.fields['name'].label = _('Nazwa')
        self.fields['description'].label = _('Opis')
        self.fields['price'].label = _('Cena')
        self.fields['image'].label = _('Zdjęcie')


class EditItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('name', 'description', 'price', 'image', 'is_sold')
        widgets = {
            'name': forms.TextInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Nazwa...')
            }),
            'description': forms.Textarea(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Opis...')
            }),
            'price': forms.TextInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Cena...')
            }),
            'image': forms.FileInput(attrs={
                'class': INPUT_CLASSES,
                'placeholder': _('Wybierz zdjęcie...')
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].label = _('Nazwa')
        self.fields['description'].label = _('Opis')
        self.fields['price'].label = _('Cena')
        self.fields['image'].label = _('Zdjęcie')
        self.fields['is_sold'].label = _('Czy sprzedane?')
