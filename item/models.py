from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='nazwa')

    class Meta:
        ordering = ('name',)
        verbose_name = 'kategoria'
        verbose_name_plural = 'kategorie'

    def __str__(self):
        return self.name


class Item(models.Model):
    category = models.ForeignKey(
        Category, related_name='items', on_delete=models.CASCADE, verbose_name='kategoria')
    name = models.CharField(max_length=255, verbose_name='nazwa')
    description = models.TextField(blank=True, null=True, verbose_name='opis')
    price = models.FloatField(verbose_name='cena')
    image = models.ImageField(upload_to='item_images',
                              null=True, verbose_name='obraz')
    is_sold = models.BooleanField(default=False, verbose_name='sprzedane')
    created_by = models.ForeignKey(
        User, related_name='items', on_delete=models.CASCADE, verbose_name='utworzone przez')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='utworzono')

    class Meta:
        verbose_name = 'przedmiot'
        verbose_name_plural = 'przedmioty'

    def __str__(self):
        return self.name
