FROM python:alpine3.19

WORKDIR /app

COPY . .

RUN pip install django \
&& apk add pkgconf\
&&  apk add mariadb-dev\
&& pip install pillow \
&& apk add build-base \
&& pip install mysql-connector-python \
&& pip install mysqlclient



EXPOSE 8000

CMD ["python3","manage.py","runserver","0.0.0.0:8000"]


